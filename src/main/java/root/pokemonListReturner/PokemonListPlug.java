package root.pokemonListReturner;

import root.resources.entity.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonListPlug extends PokemonMapper {

    @Override
    public List<Pokemon> getPokemonList(int offset) {
        Pokemon testPokemon  = new Pokemon();
        List<Pokemon> pokemonList = new ArrayList<>();

        testPokemon.setPokemonId(999);
        testPokemon.setName("StupidZavr");
        testPokemon.setUrl("http://stupidzavr.com");

        for (int i = 0; i < 20; i++) {
            pokemonList.add(testPokemon);
        }

        return pokemonList;
    }
}
