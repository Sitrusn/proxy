package root.gateway;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import root.resources.dto.PokemonPage;
import root.resources.entity.BigPokemon;

@Component
public class PokemonGateway {

    public ResponseEntity<PokemonPage> getPokemonList(int offset) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/?limit=20&offset=" + offset, HttpMethod.GET, getDefaultEntity(), PokemonPage.class);
    }

    public ResponseEntity<BigPokemon> getBigPokemon(int pokemonId){
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/" + pokemonId, HttpMethod.GET, getDefaultEntity(), BigPokemon.class);
    }

    private HttpEntity getDefaultEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("User-Agent", "Google Chrome");
        return new HttpEntity<>(httpHeaders);
    }
}