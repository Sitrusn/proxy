package root.resources.entity;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class BigPokemonListResource extends ResourceSupport {
    private BigPokemonList bigPokemonList;
}
