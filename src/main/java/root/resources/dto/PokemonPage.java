package root.resources.dto;

import lombok.Data;
import root.resources.entity.Pokemon;

import java.util.List;

@Data
public class PokemonPage {
    private int count;
    private String next;
    private String previous;
    private List<Pokemon> results;
}
