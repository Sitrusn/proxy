package root.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import root.service.TransactionTestService;

@RestController
@RequestMapping("/ut")
@AllArgsConstructor
public class TransactionTestController {

    @Autowired
    TransactionTestService transactionTestService;

    @GetMapping
    public String transactionTest() {
        transactionTestService.externalTransaction();
        return "Ok";
    }
}
