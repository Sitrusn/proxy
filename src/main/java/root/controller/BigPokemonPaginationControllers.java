package root.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import root.repository.BigPokemonRepository;
import root.assembler.BigPokemonListResourceAssembler;
import root.resources.entity.BigPokemonList;
import root.resources.entity.BigPokemonListResource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class BigPokemonPaginationControllers {

    @Autowired
    BigPokemonRepository bigPokemonRepository;

    @Autowired
    HttpSession httpSession;

    @GetMapping("/pageableview")
    public Object getPokemonsFirstPage(HttpServletRequest request) {

        httpSession.setAttribute("pageNumber", 0);
        httpSession.setAttribute("pageSize", 2);
        Pageable pageable = PageRequest.of(0, 2);
        int pagesQuantity =  bigPokemonRepository.findAll(pageable).getTotalPages();
        httpSession.setAttribute("pagesQuantity", pagesQuantity);
        BigPokemonList bigPokemonList = new BigPokemonList(bigPokemonRepository.findAll(pageable).getContent());
        return new BigPokemonListResourceAssembler().toResource(bigPokemonList);
    }

    @GetMapping("/next")
    public BigPokemonListResource getPokemonsNextPage(HttpSession httpSession) {
        int pageNumber = (int) httpSession.getAttribute("pageNumber") + 1;
        Pageable pageable = PageRequest.of(pageNumber, 2);
        BigPokemonList bigPokemonList = new BigPokemonList(bigPokemonRepository.findAll(pageable).getContent());
        return new BigPokemonListResourceAssembler(1,1).toResource(bigPokemonList);
    }

    @GetMapping("/previous")
    public BigPokemonListResource getPokemonsPreviousPage(HttpSession httpSession) {
        int pageNumber = (int) httpSession.getAttribute("pageNumber") - 1;
        Pageable pageable = PageRequest.of(pageNumber, 2);
        BigPokemonList bigPokemonList = new BigPokemonList(bigPokemonRepository.findAll(pageable).getContent());

        return new BigPokemonListResourceAssembler(1,1).toResource(bigPokemonList);
    }
}