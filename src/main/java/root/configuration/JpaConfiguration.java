package root.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import root.repository.UserRepository;
import root.resources.entity.User;

@Configuration
public class JpaConfiguration {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void setQWTypeUser(User user) {
        User newUser = new User();
        newUser.setId(1 + 1);
        newUser.setName("new" + "Some name which I don't want to see in DB");
        userRepository.save(newUser);

        userRepository.getQWUser();

    }
}
