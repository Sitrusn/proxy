package root.mapper;

import org.springframework.http.ResponseEntity;
import root.assembler.BigPokemonResourceAssembler;
import root.gateway.PokemonGateway;
import root.resources.entity.BigPokemon;
import root.resources.entity.BigPokemonResource;

public class ResponceEntityToEntityResourceMapper {

    public BigPokemonResource mapper(int bigPokemonId){

        PokemonGateway pokemonGateway = new PokemonGateway();
        BigPokemon bigPokemon = new BigPokemon();
        ResponseEntity<BigPokemon> bigPokemonEntity = pokemonGateway.getBigPokemon(bigPokemonId);

        bigPokemon.setBigPokemonId(bigPokemonEntity.getBody().getBigPokemonId());
        bigPokemon.setName(bigPokemonEntity.getBody().getName());
        bigPokemon.setAbilities(bigPokemonEntity.getBody().getAbilities());

        return new BigPokemonResourceAssembler().toResource(bigPokemon);
    }
}