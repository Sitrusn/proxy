package root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import root.repository.BigPokemonRepository;
import root.resources.entity.BigPokemon;

import java.util.Optional;

@Service
public class TransactionTestService {

    @Autowired
    BigPokemonRepository bigPokemonRepository;

    @Autowired
    TransactionTestService transactionTestService;

    @Transactional(rollbackFor = NullPointerException.class)
    public void externalTransaction() {
        bigPokemonRepository.setBulbazavr2();
        transactionTestService.nestedTransaction();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void nestedTransaction(){
        Optional<BigPokemon> bulbazavr = bigPokemonRepository.getBulbazavr();
        bulbazavr.get().getBigPokemonId();
    }
}
