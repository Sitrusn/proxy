<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PokemonList</title>
	<link rel="stylesheet" href="css/PokemonListLokh.css">
</head>

<body>
	<header>
		<div class="title">
			POKEMON LIST
		</div>
	</header>

    <form method = "get"  modelAttribute="pokemonListJSP">

	    <table class = "justTable" >
	        <tr>
		        <th>Id покемона</th>
		        <th>Имя покемона</th>
	        </tr>

            <c:forEach items="${PokemonList}" var="pokemon">
                <tr>
                    <td><input  name="pokemonId" value=${pokemon.pokemonId}    readonly</td>
                    <td><input  value=${pokemon.name}  readonly</td>
                </tr>
            </c:forEach>
   	    </table>

	    <div class="button">
	        <input type="submit" name="button" value="Previous" />
            <input type="submit" name="button" value="Next" />
	    </div>
	</form>

</body>
</html>